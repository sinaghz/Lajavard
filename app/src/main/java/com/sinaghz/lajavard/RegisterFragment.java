package com.sinaghz.lajavard;

/**
 * Created by sina on 9/9/2017.
 */
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.sinaghz.lajavard.util.Util;

import java.util.ArrayList;

public class RegisterFragment extends Fragment{

    View mainView;
    Spinner spinneryourage;
    Spinner spinnerhisage;
    ArrayList<String> yourages;
    ArrayList<String> hisages ;
    RadioGroup  yoursex;
    RadioGroup  hissex;
    RadioGroup havechilds;
    LinearLayout linearLayout;
    TextView changebleage;
    TextView changeblesex;
    TextView phonenumber;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_register, container,
                false);



        linearLayout = (LinearLayout) mainView.findViewById(R.id.linear_child);
        changebleage = (TextView) mainView.findViewById(R.id.changebleage);
        changeblesex = (TextView) mainView.findViewById(R.id.changeblesex);
        phonenumber = (TextView) mainView.findViewById(R.id.phonenumber);
        declarehisagespinner();
        declareyouragespinner();
        declareYoursex();
        declareHisSex();
        declarehaschild();

        Bundle objects = getArguments();
        if (objects.getString(Util.PHONE) != null){
            phonenumber.setText(objects.getString(Util.PHONE));
        }

        return mainView;

    }

    void declareyouragespinner(){
        spinneryourage = (Spinner) mainView.findViewById(R.id.spinneryourage);
        yourages = new ArrayList<>();
        yourages.add(getResources().getString(R.string.agerange7));
        yourages.add(getResources().getString(R.string.agerange8));
        yourages.add(getResources().getString(R.string.agerange9));
        yourages.add(getResources().getString(R.string.agerange10));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, yourages);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinneryourage.setAdapter(dataAdapter);

    }

    void declarehisagespinner(){
        spinnerhisage = (Spinner) mainView.findViewById(R.id.spinnerhisage);
        hisages = new ArrayList<>();
        hisages.add(getResources().getString(R.string.agerange1));
        hisages.add(getResources().getString(R.string.agerange2));
        hisages.add(getResources().getString(R.string.agerange3));
        hisages.add(getResources().getString(R.string.agerange4));
        hisages.add(getResources().getString(R.string.agerange5));
        hisages.add(getResources().getString(R.string.agerange6));
        hisages.add(getResources().getString(R.string.agerange7));
        hisages.add(getResources().getString(R.string.agerange8));
        hisages.add(getResources().getString(R.string.agerange9));
        hisages.add(getResources().getString(R.string.agerange10));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, hisages);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerhisage.setAdapter(dataAdapter);
       spinnerhisage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               changebleage.setText(hisages.get(position) + " " + "سالشه");
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });

    }

    void declareYoursex(){
        yoursex = (RadioGroup) mainView.findViewById(R.id.radiogroupyoursex);
        yoursex.check(R.id.radiofemale);

    }

    void declarehaschild(){
        havechilds = (RadioGroup) mainView.findViewById(R.id.radiochildren);
        havechilds.check(R.id.radioyes);
        havechilds.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (R.id.radioyes == checkedId){
                    linearLayout.setVisibility(LinearLayout.VISIBLE);
                }
                else if(R.id.radiono == checkedId){
                    linearLayout.setVisibility(LinearLayout.INVISIBLE);
                }
            }
        });

    }

    void declareHisSex(){
        hissex = (RadioGroup) mainView.findViewById(R.id.radiosex);
        hissex.check(R.id.radiogirl);

        hissex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (R.id.radiogirl == checkedId){
                    changeblesex.setText(getResources().getString(R.string.girl));
                }
                else if (R.id.radioboy == checkedId){
                    changeblesex.setText("پسره");
                }
            }
        });
    }

}
