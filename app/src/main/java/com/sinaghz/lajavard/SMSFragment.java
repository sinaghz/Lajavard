package com.sinaghz.lajavard;

/**
 * Created by sina on 9/9/2017.
 */
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sinaghz.lajavard.util.Util;

public class SMSFragment extends Fragment {

    View mainView;
    Button getCodeBtn;
    Button btnnext;
    EditText phonenumberedit;
    TextView hinttxt;
    EditText digit1;
    EditText digit2;
    EditText digit3;
    EditText digit4;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();

            try {

                if (bundle != null) {

                    final Object[] pdusObj = (Object[]) bundle.get("pdus");

                    for (int i = 0; i < pdusObj.length; i++) {

                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                        String message = currentMessage.getDisplayMessageBody();
                        if (message != null && message.length() == 4 && Util.isInteger(message)){
                            digit1.setText(message.substring(0,1));
                            digit2.setText(message.substring(1, 2));
                            digit3.setText(message.substring(2, 3));
                            digit4.setText(message.substring(3, 4));
                            btnnext.setEnabled(true);
                        }

                    }
                }

            } catch (Exception e) {
                Log.e("SmsReceiver", "Exception smsReceiver" +e);

            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_sms, container,
                false);



        getCodeBtn = (Button) mainView.findViewById(R.id.getcodebtn);
        btnnext = (Button) mainView.findViewById(R.id.btnnext);
        phonenumberedit = (EditText) mainView.findViewById(R.id.phonenumberedit);
        hinttxt = (TextView) mainView.findViewById(R.id.hinttxt);
        digit1 = (EditText) mainView.findViewById(R.id.digit1);
        digit2 = (EditText) mainView.findViewById(R.id.digit2);
        digit3 = (EditText) mainView.findViewById(R.id.digit3);
        digit4 = (EditText) mainView.findViewById(R.id.digit4);
        if (digit1.getText()!=null){
            btnnext.setEnabled(true);
        }

        phonenumberedit.setSelection((phonenumberedit.getText() != null ? phonenumberedit.getText().toString().length()  : 0));
        phonenumberedit.addTextChangedListener(new MyTextWatcher(phonenumberedit));

        getCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
                getCodeBtn.setEnabled(false);
                phonenumberedit.setEnabled(false);
                IntentFilter intentFilter=new IntentFilter();
                intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");

                getActivity().registerReceiver(receiver, intentFilter);

            }
        });

        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragment fragment = new RegisterFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Util.PHONE,phonenumberedit.getText().toString());
                fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmain, fragment).addToBackStack(null)
                        .commit();
            }
        });

        return mainView;
    }

    private void showDialog(){
        final Dialog dialog = new Dialog(getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom);
        dialog.show();

        Button btnConfirm = (Button)dialog.findViewById(R.id.btnconfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
    }

    class MyTextWatcher implements TextWatcher{
        EditText editabletxt;

        MyTextWatcher(EditText editText){
            editabletxt = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editabletxt.getText() != null && editabletxt.getText().toString().length() == 13){
                 hinttxt.setVisibility(View.INVISIBLE);
                getCodeBtn.setEnabled(true);
            }
            else {
                hinttxt.setVisibility(View.VISIBLE);
                getCodeBtn.setEnabled(false);
            }
        }
    }
}
